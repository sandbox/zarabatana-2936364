<?php

/**
 * @file
 * Rise Up theme implementation - Riseu Up Block.
 */
?>
<?php if (!isset($number)): ?> 
  <?php if (isset($sum_total)): ?>   
    <div id="riseup_container">
      <!-- @TODO - Change to dynamic values -->
      <svg width="100%" height="300px" viewbox="0 0 300 300">
        <defs>
          <marker id="arrow" markerWidth="10" markerHeight="10" refX="0" refY="3" orient="auto" markerUnits="strokeWidth">
            <path d="M0,0 L0,6 L9,3 z" fill="#f00" />
          </marker>
        </defs>
        <?php if (isset($x1)): ?>
          <line x1="<?php echo $x1; ?>" y1="<?php echo $y1; ?>" x2="250" y2="50" stroke="#000" stroke-width="5" marker-end="url(#arrow)" />
            <animate attributeName="x2" from="<?php echo $x1; ?>" to="100" begin="1s" dur="2s" /> 
            <animate attributeName="y2" from="50" to="100" begin="1s" dur="2s" />
          </line>
        <?php endif; ?>
        <?php if (isset($x2)): ?>
        <line x1="0" y1="250" x2="0" y2="250" style="stroke:rgb(0,0,0);stroke-width:20;" > 
          <animate attributeName="x2" from="<?php echo $x1; ?>" to="<?php echo $x2; ?>" begin="1s" dur="2s" fill="freeze" /> 
          <animate attributeName="y2" from="<?php echo $y1; ?>" to="<?php echo $y2; ?>" begin="1s" dur="2s" fill="freeze" /> 
        </line>
        <?php endif; ?>


      <g>
          <circle r='80' stroke="red" stroke-width="3" fill="white" cx='90' cy='120'/>
          <text x='22' y='133' fill="red" font-family="Verdana" font-size="35" visibility="visible">
              <?php echo $sum_total; ?>
              <set attributeName='visibility' begin='0s' dur='3s' to='hidden'/>
              <set attributeName='visibility' begin='5s' dur='3s' to='hidden'/>
          </text>
          <text x='22' y='133' fill="red" font-family="Verdana" font-size="35" visibility="hidden">
              <?php echo $sum_total; ?>
              <set attributeName='visibility' begin='0s' dur='3s' to='visible'/>
              <set attributeName='visibility' begin='5s' dur='3s' to='visible'/>
          </text>
          <animateMotion path='M 0 0 L 100 100' begin='0s' dur='3s' fill='freeze' />
          <animateMotion path='M 100 100 L 0 0' begin='5s' dur='3s' fill='freeze' />
      </g>


      </svg>
    </div>
  <?php endif; ?>
<?php endif; ?>
<?php if (isset($number)): ?>   
  <div id="riseup_container">
    <!-- @TODO - Change to dynamic values -->
    <svg width="100%" height="300px" viewbox="0 0 300 300">
      <text x="0" y="50" fill="red" font-family="Verdana" font-size="35"><?php echo $sum_total; ?></text>
    </svg>
  </div>
<?php endif; ?>
