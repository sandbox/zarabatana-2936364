<?php

/**
 * @file
 * Helper functions for the riseup module.
 */

/**
 * Define the necessary fields base.
 */
function _riseup_install_fields() {
  $t = get_t();
  $a = $_POST['customfield'];

  foreach ($a as $key => $value) {
    $riseup_name = $value['riseup_name'];
    $mn = preg_replace('@[^a-z0-9-]+@', '_', strtolower($riseup_name));
    $machine_name = substr($mn, 0, 12);
    $riseup_machine_name = 'riseup_' . $machine_name;
    $options = $value['options'];

    if (field_info_field($riseup_machine_name)) {
    }
    else {
      return array(
        $riseup_machine_name => array(
          'field_name' => $riseup_machine_name,
          'label' => $riseup_name,
          'type' => 'list_text',
          'settings' => array(
            'allowed_values' => drupal_map_assoc($options),
          ),
        ),
      );
    }
  }
}

/**
 * Update fields label and allowed values.
 */
function _riseup_update_fields() {
  $selectedfields_m = $_POST['riseup_load_fields'];
  $selectedfields_n = $_POST['riseupselectdfield'];

  foreach (array_combine($selectedfields_m, $selectedfields_n) as $selectedfield_m => $selectedfield_n) {

    $field = field_info_field($selectedfield_m);

    $a = $selectedfield_n['riseup_name'];
    $bb = $selectedfield_n['options_update'];
    $b = count($bb) - 1;
    $c = $selectedfield_n['options_update'];

    $allv_array = array();
    foreach ($c as $key => $value) {
      foreach ($value as $key2 => $value2) {
        $allv_array[] = $value2;
      }
    }
    $allowed_values = array_filter($allv_array);
    $field['label'] = $selectedfield_n['riseup_name'];
    $field['settings']['allowed_values'] = $allowed_values;

    if (!empty($field['settings']['allowed_values'])) {
      field_update_field($field);
    }
  }
}

/**
 * Define the field instances.
 */
function _riseup_install_instances() {
  $t = get_t();

  $riseup_selectct = variable_get('riseup_selectct', '');
  $a = $_POST['customfield'];

  foreach ($a as $key => $value) {
    $riseup_name = $value['riseup_name'];
    $mn = preg_replace('@[^a-z0-9-]+@', '_', strtolower($riseup_name));
    $machine_name = substr($mn, 0, 12);
    $riseup_machine_name = 'riseup_' . $machine_name;

    if (field_info_instance('node', $riseup_machine_name, $riseup_selectct)) {
    }
    else {
      return array(
        $riseup_machine_name => array(
          'field_name' => $riseup_machine_name,
          'type' => 'list_text',
          'label' => $riseup_name,
          'widget' => array(
            'type' => 'options_select',
          ),
          'display' => array(
            'example_node_list' => array(
              'label' => $t($riseup_name),
              'type' => 'list_text',
            ),
          ),
        ),
      );
    }
  }
}

/**
 * Create the custom node fields.
 */
function _riseup_add_ct_fields() {
  $riseup_selectct = variable_get('riseup_selectct', '');
  $a = $_POST['customfield'];

  foreach ($a as $key => $value) {
    $riseup_name = $value['riseup_name'];
    $mn = preg_replace('@[^a-z0-9-]+@', '_', strtolower($riseup_name));
    $machine_name = substr($mn, 0, 12);
    $riseup_machine_name = 'riseup_' . $machine_name;

    if (field_info_instance('node', $riseup_machine_name, $riseup_selectct)) {
      drupal_set_message('FAILED! This field already exists on this content type!');
    }
    else {
      foreach (_riseup_install_fields() as $field) {
        field_create_field($field);
      }
    }
    foreach (_riseup_install_instances() as $fieldinstance) {
      $fieldinstance['entity_type'] = 'node';
      $fieldinstance['bundle'] = $riseup_selectct;
      field_create_instance($fieldinstance);
    }
  }
}

/**
 * Delete fields when needed.
 */
function _riseup_delete_ct_fields() {
  foreach (array_keys(_riseup_install_fields()) as $field) {
    field_delete_field($field);
  }
  $instances = field_info_instances('node', $riseup_rselectct);
  foreach ($instances as $instance_name => $fieldinstance) {
    field_delete_instance($fieldinstance);
  }
}

/**
 * Create the custom user fields.
 */
function _add_user_fields() {
  $a = $_POST['customfield'];

  foreach ($a as $key => $value) {
    $riseup_name = $value['riseup_name'];
    $mn = preg_replace('@[^a-z0-9-]+@', '_', strtolower($riseup_name));
    $machine_name = substr($mn, 0, 12);
    $riseup_machine_name = 'riseup_' . $machine_name;

    $customfield = array(
      'field_name' => $riseup_machine_name,
      'entity_type' => 'user',
      'bundle' => 'user',
      'label' => $riseup_name,
      'widget' => array(
        'type' => 'list_text',
      ),
    );

    if (field_info_instance('user', $riseup_machine_name, 'user')) {
      drupal_set_message('FAILED! This field already exists in the user profile!');
    }
    else {
      field_create_instance($customfield);
    }
  }
}

/**
 * Helper function to get machine_name of selected content type.
 */
function _riseup_get_machine_name() {
  $riseup_selectct = variable_get('riseup_selectct', '');
  $machine_name = db_select('node_type', 'n')
    ->fields('n', array('type'))
    ->condition('name', $riseup_selectct)
    ->execute()->fetchField();
  return $machine_name;
}
