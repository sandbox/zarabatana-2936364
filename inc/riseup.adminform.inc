<?php

/**
 * @file
 * Administration functions for the riseup module.
 */

/**
 * Provide a description to each option in the element.
 */
function _option_descriptions($element, &$form_state) {
  foreach (element_children($element) as $key) {
  }
  return $element;
}

/**
 * Form submit.
 */
function riseup_admin_form_submit(&$form, &$form_state) {

  form_state_values_clean($form_state);
  $output = t("Fields form has been submitted.<br>");

  if (empty($form_state['customfield'])) {
    $form_state['customfield'] = 1;
  }

  for ($i = 1; $i <= $form_state['customfield']; $i++) {
    $output .= t("Attempt to create new fields: @num<br>Name: @riseup_name<br>",
      array(
        '@num' => $i,
        '@riseup_name' => $form_state['values']['customfield'][$i]['riseup_name'],
      )
    ) . ' ';

    // When the Label is empty don't create the field.
    if (empty($form_state['values']['customfield'][$i]['riseup_name'])) {
      drupal_set_message('FAILED! One or more fields are empty!');
    }
    else {
      drupal_set_message($output);
      _riseup_add_ct_fields();
      _add_user_fields();
      // variable_set('riseup_load_fields', $form_state['customfield']);.
      $form_state['values']['customfield'] = '';
      $x = 1;
    }
    $a = $_POST['customfield'][$i]['riseup_name'];

    // variable_set('riseup_load_fields',
    // $form_state['customfield'][$i]['riseup_name']);.
  }

  // Remove next two lines to get content from
  // previous submit.
  $form_state['input'] = array();
  $form_state['rebuild'] = TRUE;

  // @todo
  // return system_settings_form($form);
  if (is_array(variable_get('riseup_load_fields'))) {
    $selectedfields = array_filter(variable_get('riseup_load_fields'));
    variable_set('selectedfields', $selectedfields);
  }

}

/**
 * Form update submit.
 */
function riseup_admin_form_update_submit() {

  // form_state_values_clean($form_state);
  $output = t("Fields form has been submitted.<br>");

  $form_state['rebuild'] = TRUE;
  _riseup_update_fields();
}

/**
 * Add field.
 */
function riseup_admin_form_add_field($form, &$form_state) {
  // Everything in $form_state is persistent, so we'll just use
  // $form_state['add_name'].
  $form_state['customfield']++;

  // Setting $form_state['rebuild'] = TRUE causes the form to be rebuilt again.
  $form_state['rebuild'] = TRUE;
}

/**
 * Add option.
 */
function riseup_admin_form_add_option($form, &$form_state) {
  // Everything in $form_state is persistent, so we'll just use.
  // $form_state['add_name'].
  // for ($i = 1; $i <= $form_state['customfield']; $i++) {.
  $form_state['num_names']++;
  // }.
  // Setting $form_state['rebuild'] = TRUE causes the form to be rebuilt again.
  $form_state['rebuild'] = TRUE;
}

/**
 * Add option update.
 */
function riseup_admin_form_add_option_update($form, &$form_state) {
  // Everything in $form_state is persistent, so we'll just use.
  // $form_state['add_name'].
  // for ($i = 1; $i <= $form_state['customfield']; $i++) {.
  $form_state['options_update_add_option']++;
  // }.
  // Setting $form_state['rebuild'] = TRUE causes the form to be rebuilt again.
  $form_state['rebuild'] = TRUE;
}

/**
 * Remove field.
 */
function riseup_admin_form_remove_field($form, &$form_state) {
  if ($form_state['customfield'] > 1) {
    $form_state['customfield']--;
  }

  // Setting $form_state['rebuild'] = TRUE causes the form to be rebuilt again.
  $form_state['rebuild'] = TRUE;
}

/**
 * Remove option.
 */
function riseup_admin_form_remove_option($form, &$form_state) {
  if ($form_state['num_names'] > 1) {
    $form_state['num_names']--;
  }

  // Setting $form_state['rebuild'] = TRUE
  // causes the form to be rebuilt again.
  $form_state['rebuild'] = TRUE;
}

/**
 * Remove option update.
 */
function riseup_admin_form_remove_option_update($form, &$form_state) {
  if ($form_state['options_update_add_option'] > 1) {
    $form_state['options_update_add_option']--;
  }

  // Setting $form_state['rebuild'] = TRUE
  // causes the form to be rebuilt again.
  $form_state['rebuild'] = TRUE;
}

/**
 * Delete fields when needed.
 */
function riseup_admin_form_delete_fields(&$form, &$form_state) {
  // form_state_values_clean($form_state);
  $deletefields = $form['riseup_delete_fields']['#value'];

  if (!is_array($deletefields)) {
    field_delete_field($deletefields);
  }
  else {
    foreach ($deletefields as $deletefield => $value) {
      field_delete_field($deletefield);
    }
  }

  // Test and improve to remove the necessary only.
  variable_del('riseup_load_fields');
  // To remove after test.
  // _riseup_get_selectd_fields();
}

/**
 * Implements hook_form_alter().
 */
function riseup_form_alter(&$form, &$form_state, $form_id) {
  $selectct = variable_get('riseup_selectct');
  $loadfields = variable_get('riseup_load_fields');
  if ((!empty($selectct)) && (!empty($loadfields))) {
    if ($form_id == 'riseup_admin_form') {
      $form['riseup_selectct']['#access'] = 0;
    }
  }
}

/**
 * Check riseup fields.
 */
function _riseup_check_fields() {
  if (!empty(variable_get('riseup_load_fields'))) {
    $checkfields = array();
    $checkfields = variable_get('riseup_load_fields');
    return $checkfields;
  }
  else {
    variable_set('riseup_load_fields', '');
  }
}
