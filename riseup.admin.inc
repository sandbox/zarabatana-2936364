<?php

/**
 * @file
 * Administration form for the riseup module.
 */

// Include the file with the submission handlers.
require_once dirname(__FILE__) . '/inc/riseup.adminform.inc';

/**
 * Menu callback: options for riseup module.
 */
function riseup_admin_form($form, &$form_state) {
  $form = array();
  global $user;

  $form['#tree'] = TRUE;

  $form['riseup_selectct'] = array(
    '#title' => t('Content type list'),
    '#description' => "STEP 1 - Select the desired content type.",
    '#type' => 'select',
    '#options' => drupal_map_assoc(node_type_get_names()),
    '#default_value' => variable_get('riseup_selectct'),
  );

  $fields = field_info_instances('node', variable_get('riseup_selectct'));

  $options = array();
  foreach ($fields as $field_name => $value) {
    $field = field_info_field($field_name);
    $fname = $field['field_name'];
    if (strpos($fname, 'riseup_') !== FALSE) {
      if (!empty($field['label'])) {
        $fl = $field['label'];
      }
      else {
        $fl = $fname;
      }
      $options[$fname] = $fl;
    }
  }

  if (!empty(variable_get('riseup_selectct'))) {
    if (!empty($options)) {
      $form['riseup_load_fields'] = array(
        '#title' => 'ADD fields to compare (' . variable_get('riseup_selectct', 3) . ' content type).',
        '#type' => 'checkboxes',
        '#description' => t('Select the fields you would like to add.'),
        // Before was drupal_map_assoc($options).
        '#options' => $options,
        // Bug with next line.
        '#default_value' => variable_get('riseup_load_fields'),
        // '#options_descriptions' => array($fname => 'Description 1'),.
        '#after_build' => array('_option_descriptions'),
      );
      $form['riseup_display_mode'] = array(
        '#title' => t('Display mode'),
        '#type' => 'radios',
        '#description' => 'Choose how you want to display the results.',
        '#options' => array(
          'graphic' => 'Graphic',
          'number' => 'Just text',
        ),
        '#default_value' => variable_get('riseup_display_mode'),
      );
    }
    if (is_array(variable_get('riseup_load_fields'))) {
      $selectedfields = array_filter(variable_get('riseup_load_fields'));
      $seletedcount = count($selectedfields);
    }
    if (!empty($selectedfields)) {

      $form['description'] = array(
        '#type' => 'item',
        '#title' => t('Update fields with new values or add more and update both'),
      );

      if (empty($form_state['riseupselectdfield'])) {
        $form_state['riseupselectdfield'] = 1;
      }
      $s = 1;
      $account = user_load($GLOBALS['user']->uid);
      foreach ($selectedfields as $selectedfield => $value) {
        $infofields = field_info_field($selectedfield);
        $infofield = $infofields['label'];
        $infofieldmn = $infofields['field_name'];

        $all_fields_on_my_website = field_info_fields();
        $allowed_values = list_allowed_values($all_fields_on_my_website[$infofieldmn]);

        $form['riseupselectdfield'][$s] = array(
          '#type' => 'fieldset',
          '#title' => t('Field #@num', array('@num' => $s)) . ' - ' . $infofield,
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
        );

        $form['riseupselectdfield'][$s]['riseup_name'] = array(
          '#type' => 'textfield',
          '#title' => t('Label'),
          '#size' => 60,
          '#maxlength' => 255,
          // '#description' => t("Add ."),.
          '#default_value' => $infofield,
        );

        if (empty($form_state['options_update'])) {
          $form_state['options_update'] = 1;
        }

        $o = 1;
        for ($o = 1; $o <= $form_state['options_update']; $o++) {

          foreach ($allowed_values as $key => $value) {
            $form['riseupselectdfield'][$s]['options_update'][$o][$key] = array(
              '#type' => 'textfield',
              '#title' => t('Option #@num', array('@num' => $o)),
              '#default_value' => $value,
            );
            $o++;
          }

          if (empty($form_state['options_update_add_option'])) {
            $form_state['options_update_add_option'] = 1;
          }

          for ($f = 1; $f <= $form_state['options_update_add_option']; $f++) {
            $form['riseupselectdfield'][$s]['options_update'][$o][$f] = array(
              '#type' => 'textfield',
              '#title' => t('Option to add #@num', array('@num' => $o++)),
            );
          }

          // Adds "Add another option" button.
          $form['riseupselectdfield'][$s]['options_update'][$o][$key]['add_another_option_update'] = array(
            '#type' => 'submit',
            '#value' => t('Add another option update'),
            '#submit' => array('riseup_admin_form_add_option_update'),
          );

          if ($form_state['options_update_add_option'] > 1) {
            $form['riseupselectdfield'][$s]['options_update'][$o][$f]['remove_option_update'] = array(
              '#type' => 'submit',
              '#value' => t('Remove latest option update'),
              '#submit' => array('riseup_admin_form_remove_option_update'),
              // Since we are removing a name, don't validate until later.
              '#limit_validation_errors' => array(),
            );
          }
          $s++;
        }
      }

      $form['submit_update'] = array(
        '#type' => 'submit',
        '#value' => t('Update fields'),
        '#submit' => array('riseup_admin_form_update_submit'),
      );
    }

    if (empty($form_state['customfield'])) {
      $form_state['customfield'] = 1;
    }
    $form_state['customfield'] = count(variable_get('selectedfields')) + 1;
    $rsselectedfields = count(variable_get('selectedfields')) + 1;
    if (!isset($rsselectedfields)) {
      $rsselectedfields = 1;
    }
    for ($i = $rsselectedfields; $i <= $form_state['customfield']; $i++) {

      $form['customfield'][$i] = array(
        '#type' => 'fieldset',
        '#title' => t('CREATE A NEW FIELD'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );

      $form['customfield'][$i]['riseup_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Label'),
        '#size' => 60,
        '#maxlength' => 255,
        '#description' => t("Add custom fields."),
        '#required' => FALSE,
      );

      if (empty($form_state['num_names'])) {
        $form_state['num_names'] = 1;
      }

      for ($x = 1; $x <= $form_state['num_names']; $x++) {

        $form['customfield'][$i]['options'][$x] = array(
          '#type' => 'textfield',
          '#title' => t('Option #@num', array('@num' => $x)),
          '#required' => FALSE,
        );
      }

      // Adds "Add another option" button.
      $form['customfield'][$i]['options'][$x]['add_another_option'] = array(
        '#type' => 'submit',
        '#value' => t('Add another option'),
        '#submit' => array('riseup_admin_form_add_option'),
      );

      if ($form_state['num_names'] > 1) {
        $form['customfield'][$i]['options'][$x]['remove_option'] = array(
          '#type' => 'submit',
          '#value' => t('Remove latest option'),
          '#submit' => array('riseup_admin_form_remove_option'),
          // Since we are removing a name, don't validate until later.
          '#limit_validation_errors' => array(),
        );
      }
    }
    // End of complete fieldset - 23072017.
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Create fields'),
      '#submit' => array('riseup_admin_form_submit'),
    );
    // 2018
    // $form['submit']['#ajax'] = array(
    // 'callback' => 'riseup_ajax_submit',
    // 'progress' => array('type' => 'throbber'),
    // 'effect' => 'fade',
    // 'wrapper' => 'riseup_admin_form',
    // );
    // Adds "Add another field" button.
    $form['add_field'] = array(
      '#type' => 'submit',
      '#value' => t('Add another field'),
      '#submit' => array('riseup_admin_form_add_field'),
    );

    // If we have more than one name, this button allows removal of the
    // last name.
    if ($form_state['customfield'] > 1) {
      $form['remove_field'] = array(
        '#type' => 'submit',
        '#value' => t('Remove latest field'),
        '#submit' => array('riseup_admin_form_remove_field'),
        // Since we are removing a name, don't validate until later.
        '#limit_validation_errors' => array(),
      );
    }

    if (!empty($options)) {
      $form['riseup_delete_fields'] = array(
        '#title' => 'DELETE fields (' . variable_get('riseup_selectct', 3) . ' content type).',
        '#type' => 'checkboxes',
        '#description' => t('Select the fields you would like to delete.'),
        // Before was drupal_map_assoc($options).
        '#options' => $options,
      );

      $form['delete_fields'] = array(
        '#type' => 'submit',
        '#value' => t('Delete'),
        '#submit' => array('riseup_admin_form_delete_fields'),
      );
      if (is_array(variable_get('riseup_load_fields'))) {
        $selectedfields = array_filter(variable_get('riseup_load_fields'));
        variable_set('selectedfields', $selectedfields);
      }
      if (is_array(variable_get('riseup_delete_fields'))) {
        $deletefields = array_filter(variable_get('riseup_delete_fields'));
        variable_set('deletefields', $deletefields);
      }
    }
  }
  return system_settings_form($form);
}
